package com.neppo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.neppo.dto.DistanciaRequest;
import com.neppo.entity.Pessoa;

/**
 * Classe de acesso a Base de dados, onde estao implementados metodos
 * especificos para manipulacao de objetos do tipo {@link Pessoa}.
 *
 * @author bruno
 */
@Repository
@Transactional
public class PessoaDAO extends GenericDAO<Pessoa> implements IPessoaDAO {

	public final static int NOVE = 9;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xy.inc.dao.IPessoaDAO#readAllPessoa()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Pessoa> readAllPessoa() throws DAOException {

		try {
			Criteria criteria = getCurrentSession()
					.createCriteria(Pessoa.class);
			return criteria.list();
		} catch (HibernateException ex) {
			throw new DAOException(ex);
		} catch (Exception ex) {
			throw new DAOException(ex);
		}

	}

	@Override
	public List<Object[]> getDadosSexo() throws DAOException {

		try {
			Criteria criteria = getCurrentSession()
					.createCriteria(Pessoa.class);
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.groupProperty("sexo"));
			projList.add(Projections.count("sexo"));
			criteria.setProjection(projList);
			return criteria.list();
		} catch (HibernateException ex) {
			throw new DAOException(ex);
		} catch (Exception ex) {
			throw new DAOException(ex);
		}
	}

	@Override
	public List<Integer> getDadosIdade() throws DAOException{
		List<Integer> resultado = new ArrayList<Integer>();
		
		Criteria criteriaMenorQueNove = getCurrentSession().createCriteria(Pessoa.class);
		criteriaMenorQueNove.add(Restrictions.le("idade", 9)).setProjection(Projections.count("idade"));
		List<Integer> menorQueNove = criteriaMenorQueNove.list();
		resultado.addAll(menorQueNove);
		
		Criteria criteriaMaiorQueDez = getCurrentSession().createCriteria(Pessoa.class);
		criteriaMaiorQueDez.add(Restrictions.and(Restrictions.ge("idade", 10),Restrictions.le("idade", 19))).setProjection(Projections.count("idade"));
		List<Integer> maiorQueDez = criteriaMaiorQueDez.list();
		resultado.addAll(maiorQueDez);
		
		Criteria criteriaMaiorQueDesenove = getCurrentSession().createCriteria(Pessoa.class);
		criteriaMaiorQueDesenove.add(Restrictions.and(Restrictions.ge("idade", 20),Restrictions.le("idade", 29))).setProjection(Projections.count("idade"));
		List<Integer> maiorQueDesenove = criteriaMaiorQueDesenove.list();
		resultado.addAll(maiorQueDesenove);
		
		Criteria criteriaMaiorQueVinteENove = getCurrentSession().createCriteria(Pessoa.class);
		criteriaMaiorQueVinteENove.add(Restrictions.and(Restrictions.ge("idade", 30),Restrictions.le("idade", 39))).setProjection(Projections.count("idade"));
		List<Integer> maiorQueVinteENove = criteriaMaiorQueVinteENove.list();
		resultado.addAll(maiorQueVinteENove);
		
		Criteria criteriaMaiorQueQuarenta = getCurrentSession().createCriteria(Pessoa.class);
		criteriaMaiorQueQuarenta.add(Restrictions.ge("idade", 40)).setProjection(Projections.count("idade"));
		List<Integer> maiorQueQuarenta = criteriaMaiorQueQuarenta.list();
		resultado.addAll(maiorQueQuarenta);
		
		return resultado;
	}

}
