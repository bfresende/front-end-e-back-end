import { Component, OnInit } from '@angular/core';
import { Pessoa } from '../entity/Pessoa';
import { PessoaService } from 'app/pessoa/pessoa.service';

@Component({
  selector: 'app-pessoa',
  templateUrl: './pessoa.component.html',
  styleUrls: ['./pessoa.component.css']
})
export class PessoaComponent implements OnInit {
  pessoas: Array<Pessoa>;
  pessoaRequest = new Pessoa();
  pessoaResponse = new Pessoa();
  pessoaEdit = new Pessoa();
  newPessoaRequest = new Pessoa();
  msg: string;
  constructor(private pessoaService: PessoaService) { }

  ngOnInit() {
    this.readAll();
  }
  readAll() {
    this.pessoaService.readAll()
      .then(
        data => {
          this.pessoas = data;
        }
      );
  }
  create(pessoa: Pessoa) {
    this.pessoaService.create(pessoa)
      .then(
        data => {
          this.pessoaResponse = data[1];
          this.msg = data[2];
          document.getElementById('successCreate').removeAttribute('hidden');
          this.readAll();
        }
      );
  }
  update(pessoa: Pessoa) {
    this.pessoaService.update(pessoa)
      .then(
        data => {
          this.pessoaResponse = data[1];
          this.msg = data[2];

          document.getElementById('successEdit').removeAttribute('hidden');
        }
      );
  }
  delete(pessoa: Pessoa) {
    this.pessoaService.delete(pessoa).then(
      data => {
        this.msg = data;
      }
    );
    this.readAll();
  }
}
