import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from 'rxjs';
import { URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Injectable()
export class GraficoService {

    restURL = 'http://localhost:9999/api/grafico';

    constructor(private http: Http) {

    }
    geraGraficoSexo(): Promise<any> {
        return this.http.get(this.restURL + '/sexo')
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    geraGraficoIdade(): Promise<any> {
        return this.http.get(this.restURL + '/idade')
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
