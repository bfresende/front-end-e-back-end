import { Component, OnInit } from '@angular/core';
import { GraficoService } from 'app/grafico/grafico.service';
import { Chart } from 'chart.js';
@Component({
  selector: 'app-grafico',
  templateUrl: './grafico.component.html',
  styleUrls: ['./grafico.component.css']
})
export class GraficoComponent implements OnInit {
  chartSexo: any;
  chartIdade: any;
  qtdFeminino: Array<any>;
  qtdMasculino: Array<any>;
  idades: Array<number>;
  constructor(private graficoService: GraficoService) { }
  ngOnInit() {
    this.pegaDadosSexo();
    this.pegaDadosIdades();
  }
  montaGraficoSexo() {
    const canvas = <HTMLCanvasElement>document.getElementById('canvasSexo');
    const ctx = canvas.getContext('2d');
    this.chartSexo = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: [this.qtdMasculino[0], this.qtdFeminino[0]],
        datasets: [
          {  
            data: [this.qtdMasculino[1], this.qtdFeminino[1]],
            backgroundColor: [
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)'
            ]
          }
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
  montaGraficoIdade() {
    const canvas = <HTMLCanvasElement>document.getElementById('canvasIdade');
    const ctx = canvas.getContext('2d');
    this.chartSexo = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ['0 a 9', '10 a 19', '20 a 29', '30 a 39', 'Maior que 40'],
        datasets: [
          {
            data: this.idades,
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)'
            ]
          }
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
  pegaDadosSexo() {
    this.graficoService.geraGraficoSexo()
      .then(
      data => {
        this.qtdMasculino = data[1];
        this.qtdFeminino = data[0];
        this.montaGraficoSexo();
      }
      );
  }
  pegaDadosIdades() {
    this.graficoService.geraGraficoIdade()
      .then(
      data => {
        this.idades = data;
        this.montaGraficoIdade();
      }
      );
  }
}
