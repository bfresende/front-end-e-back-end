import { NeppoClientPage } from './app.po';

describe('neppo-client App', function() {
  let page: NeppoClientPage;

  beforeEach(() => {
    page = new NeppoClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
