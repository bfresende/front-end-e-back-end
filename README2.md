Os serviços foram desenvolvidos com Java utilizando JPA/Hibernate, Spring boot e o banco de dados Postgresql, portanto para rodar a aplicação é necessário ter um banco de dados Postgresql, criar uma base de dados com o nome "neppo" com owner postgres e senha "senha" e rodar o seguinte script para criar a tabela que será usada:

CREATE TABLE public.tb_pessoa
(
    cod_pessoa integer NOT NULL,
    endereco character varying(100) COLLATE pg_catalog."default" NOT NULL,
    idade integer NOT NULL,
    nom_pessoa character varying(100) COLLATE pg_catalog."default" NOT NULL,
    sexo character varying(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tb_pessoa_pkey PRIMARY KEY (cod_pessoa),
    CONSTRAINT unq_nom_pessoa UNIQUE (nom_pessoa)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_pessoa
    OWNER to postgres;
	
Feito isso, é necessário realizar o checkout do projeto e inicializar a aplicação com a classe NeppoApplication que possui o método main.

O client foi desenvolvido com Angular 5, então é necessário possuir o Node.js, realizar checkout do projeto, digitar o comando "npm install" e logo após a instalação de todas as dependências o comando "ng serve" para inicializar a aplicação.